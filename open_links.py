#!/usr/bin/env python

import webbrowser

links='''https://docs.python.org/3/
https://golang.org/pkg/
https://gobyexample.com/
https://quii.gitbook.io/learn-go-with-tests/
https://golang.org/doc/effective_go.html
https://github.com/garabik/grc
https://github.com/dominikh/go-mode.el
https://mholt.github.io/json-to-go/
https://github.com/bmuschko/go-testing-frameworks
http://govspy.peterbe.com/
https://www.jetbrains.com/lp/devecosystem-2019/'''

[webbrowser.open_new_tab(link) for link in links.split('\n')]
