#!/usr/bin/env python

def say(how_many_times=3):
    return 'Ni!' * how_many_times

def question_for_the_king():  # pragma: no cover
    print('WHAT is the airspeed velocity of an unladen swallow?')

if __name__ =='__main__':  # pragma: no cover
    print(say())
    question_for_the_king()
    
