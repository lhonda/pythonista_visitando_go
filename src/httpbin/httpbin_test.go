package main

import (
	"net/http"
	"testing"
)

type mockedChecklist struct {
}

func (m *mockedChecklist) Do(rq *http.Request) (r *http.Response, e error) {
	r = &http.Response{
		StatusCode: 400,
		Status:     "400 Bad request",
	}
	return
}

func TestSendToAPIShouldFail(t *testing.T) {
	client := &mockedChecklist{}

	data := &Data{
		Slideshow: Slideshow{
			Author: "Erik Idle",
			Title:  "Ni!",
			Date:   "1970-01-01",
			Slides: []Slide{
				{
					Title: "Pythonista and Go",
					Type:  "graphic",
					Items: []string{"Python", "Go"},
				},
			},
		},
	}

	expected := http.Response{
		StatusCode: http.StatusBadRequest,
	}

	result, _ := sendToAPI(data, client)

	if result.StatusCode != expected.StatusCode {
		t.Errorf("got '%v' want '%v'", result.StatusCode, expected.StatusCode)
	}
}
