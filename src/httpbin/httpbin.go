package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

/*

This code shows up a mock example.

You can generate your Go structs from JSON structs using this website: https://mholt.github.io/json-to-go/

*) Mock a method:
In your code:
1. declare a var X to be mocked
2. Create an interface which mimics the method you want to mock
3. replace declarations of your implementation by the interface above

In you test:
1. create a generic mocked struct and a mocked method
2. use this mocked struct as the receiver of your mock method

*/

// HTTPClient provides identical net/http Do's method interface
type HTTPClient interface {
	Do(rq *http.Request) (r *http.Response, e error)
}

// Data struct groups Slideshow struct
type Data struct {
	Slideshow Slideshow `json:"slideshow"`
}

// Slide provides information about each slide
type Slide struct {
	Title string   `json:"title"`
	Type  string   `json:"type"`
	Items []string `json:"items"`
}

// Slideshow is a collection of slides
type Slideshow struct {
	Author string  `json:"author"`
	Date   string  `json:"date"`
	Slides []Slide `json:"slides"`
	Title  string  `json:"title"`
}

var (
	httpClient  HTTPClient
	buildNumber string
)

func sendToAPI(data *Data, httpClient HTTPClient) (r *http.Response, err error) {
	b, _ := json.Marshal(data)

	url := "https://httpbin.org/post"

	content := bytes.NewBuffer(b)

	req, _ := http.NewRequest(http.MethodPost, url, content)

	req.Header.Add("Content-type", "application/json")

	r, err = httpClient.Do(req)

	if r.StatusCode != http.StatusOK {
		log.Println("Error sending data:", string(b))
	}

	return
}

func main() {
	fmt.Println("buildNumber:", buildNumber)
	data := &Data{
		Slideshow: Slideshow{
			Author: "Erik Idle",
			Title:  "Ni!",
			Date:   "1970-01-01",
			Slides: []Slide{
				{
					Title: "Pythonista visitando Go",
					Type:  "graphic",
					Items: []string{"Python", "Go"},
				},
			},
		},
	}

	httpClient := &http.Client{}
	r, _ := sendToAPI(data, httpClient)

	body, _ := ioutil.ReadAll(r.Body)
	fmt.Println(string(body))
}
