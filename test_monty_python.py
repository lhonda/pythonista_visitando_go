#!/usr/bin/env python

"""
This is an example of function parameter default value and
this test suite is used to show how pragma: no cover comment works with test coverage
"""

from monty_python import say

def test_say_should_succeed_3x():
    response = say()

    expected = 'Ni!' * 3

    assert expected == response

def test_say_should_succeed_5x():
    response = say(how_many_times=5)

    expected = 'Ni!' * 5

    assert expected == response
    
